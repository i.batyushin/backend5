<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('passw', '', 100000);
    if (!empty($_COOKIE['passw'])) {
      $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['passw']));
    }
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
  }
 
  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['field-name-1'] = !empty($_COOKIE['field-name-1_error']);
  $errors['field-email'] = !empty($_COOKIE['field-email_error']);
  $errors['field-date'] = !empty($_COOKIE['field-date_error']);
  $errors['field-name-3'] = !empty($_COOKIE['field-name-3_error']);
  $errors['field-name-2'] = !empty($_COOKIE['field-name-2_error']);
  $errors['field-name-4'] = !empty($_COOKIE['field-name-4_error']);
  $errors['radio-group-2'] = !empty($_COOKIE['radio-group-2_error']);
  $errors['radio-group-1'] = !empty($_COOKIE['radio-group-1_error']);
  
  // TODO: аналогично все поля.

  // Выдаем сообщения об ошибках.
  if ($errors['field-name-1']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('field-name-1_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните имя корректно.</div>';
  }
  // TODO: тут выдать сообщения об ошибках в других полях.
  if ($errors['field-email']) {
    setcookie('field-email_error', '', 100000);
    $messages[] = '<div class="error">Заполните электронный адрес корректно.</div>';
  }
  if ($errors['field-date']) {
    setcookie('field-date_error', '', 100000);
    $messages[] = '<div class="error">Заполните дату.</div>';
  }
  if ($errors['field-name-3']) {
    setcookie('field-name-3_error', '', 100000);
    $messages[] = '<div class="error">Выберите суперспособность.</div>';
  }
  if ($errors['field-name-2']) {
    setcookie('field-name-2_error', '', 100000);
    $messages[] = '<div class="error">Заполните информацию о себе через 10 лет.</div>';
  }
  if ($errors['field-name-4']) {
    setcookie('field-name-4_error', '', 100000);
    $messages[] = '<div class="error">Выберите пиццу.</div>';
  }
  if ($errors['radio-group-2']) {
    setcookie('radio-group-2_error', '', 100000);
    $messages[] = '<div class="error">Поставьте оценку.</div>';
  }
  if ($errors['radio-group-1']) {
    setcookie('radio-group-1_error', '', 100000);
    $messages[] = '<div class="error">Укажите пол.</div>';
  }

  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  $values['field-name-1'] = empty($_COOKIE['field-name-1_value']) ? '' : $_COOKIE['field-name-1_value'];
  // TODO: аналогично все поля.
  $values['field-email'] = empty($_COOKIE['field-email_value']) ? '' : $_COOKIE['field-email_value'];
  $values['field-date'] = empty($_COOKIE['field-date_value']) ? '' : $_COOKIE['field-date_value'];
  $values['field-name-3'] = empty($_COOKIE['field-name-3_value']) ? '' : $_COOKIE['field-name-3_value'];
  $values['field-name-2'] = empty($_COOKIE['field-name-2_value']) ? '' : $_COOKIE['field-name-2_value'];
  $values['field-name-4'] = empty($_COOKIE['field-name-4_value']) ? '' : $_COOKIE['field-name-4_value'];
  $values['radio-group-2'] = empty($_COOKIE['radio-group-2_value']) ? '' : $_COOKIE['radio-group-2_value'];
  $values['radio-group-1'] = empty($_COOKIE['radio-group-1_value']) ? '' : $_COOKIE['radio-group-1_value'];
  
$kosyak=false;
foreach($errors as $elem)
  if($elem)
    $kosyak=true;
    if ($kosyak && !empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // TODO: загрузить данные пользователя из БД
    // и заполнить переменную $values,
    // предварительно санитизовав.
    $user = 'u24234';
    $pass = '43523453';
    $db = new PDO('mysql:host=localhost;dbname=u24234', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $uid=$_SESSION['uid'];
    $result=$db->query("SELECT name, email, DateOfBirth, SPower, After10years, OcenkaPitsi, Gender FROM application_v2 WHERE id=$uid");
    foreach($result as $elem)
      {
        $values['field-name-1'] =strip_tags($elem['field-name-1']);
        $values['field-email'] =strip_tags($elem['field-email']);
        $values['field-date'] =strip_tags($elem['field-date']);
        $values['field-name-3'] =strip_tags($elem['field-name-3']);
        $values['field-name-2'] =strip_tags($elem['field-name-2']);
        $values['radio-group-2'] =strip_tags($elem['radio-group-2']);
        $values['radio-group-1'] =strip_tags($elem['radio-group-1']);
      }
      $result=$db->query("SELECT nomer_pitsi FROM Pizza WHERE nomer_pitsi=$uid");
      $sila=array();
      foreach($result as $elem){
        $sila[]=(int)strip_tags($elem['nomer_pitsi']);
      }
      $super=implode('', $sila);
      $values['field-name-4']=$super;
    printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
  }

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['field-name-1']) || preg_match('/[^(\x7F-\xFF)|(\s)]/', $_POST['field-name-1'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('field-name-1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('field-name-1_value', $_POST['field-name-1'], time() + 30 * 24 * 60 * 60);
  }

// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************
if (empty($_POST['field-email'])) {
  setcookie('field-email_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('field-email_value', $_POST['field-email'], time() + 30 * 24 * 60 * 60);
} if (empty($_POST['field-date'])) {
  setcookie('field-date_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('field-date_value', $_POST['field-date'], time() + 30 * 24 * 60 * 60);
} if (empty($_POST['field-name-3'])) {
  setcookie('field-name-3_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('field-name-3_value', $_POST['field-name-3'], time() + 30 * 24 * 60 * 60);
} if (empty($_POST['field-name-2'])) {
  setcookie('field-name-2_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('field-name-2_value', $_POST['field-name-2'], time() + 30 * 24 * 60 * 60);
} if (empty($_POST['field-name-4'])) {
  setcookie('field-name-4_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('field-name-4_value', $_POST['field-name-4'], time() + 30 * 24 * 60 * 60);
} if (empty($_POST['radio-group-2'])) {
  setcookie('radio-group-2_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('radio-group-2_value', $_POST['radio-group-2'], time() + 30 * 24 * 60 * 60);
} if (empty($_POST['radio-group-1'])) {
  setcookie('radio-group-1_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('radio-group-1_value', $_POST['radio-group-1'], time() + 30 * 24 * 60 * 60);
}

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('field-name-1_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
    setcookie('field-email_error', '', 100000);
    setcookie('field-date_error', '', 100000);
    setcookie('field-name-3_error', '', 100000);
    setcookie('field-name-2_error', '', 100000);
    setcookie('field-name-4_error', '', 100000);
    setcookie('radio-group-2_error', '', 100000);
    setcookie('radio-group-1_error', '', 100000);
  }
  $user = 'u24234';
  $pass = '43523453';
  $db = new PDO('mysql:host=localhost;dbname=u24234', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
        $uid=$_SESSION['uid'];
        $stmt = $db->prepare("UPDATE application_v2 SET name = ?, email = ?, DateOfBirth = ?, SPower = ?, After10years = ?, OcenkaPitsi = ?, Gender = ? WHERE id=$uid");
        $stmt -> execute([$_POST['field-name-1'],$_POST['field-email'],$_POST['field-date'],$_POST['field-name-3'],$_POST['field-name-2'],$_POST['radio-group-2'],$_POST['radio-group-1']]);
        $db->query("DELETE FROM Pizza WHERE id=$uid");
        $stmt = $db->prepare("INSERT INTO Pizza SET user_id=?,nomer_pitsi=?");
        foreach($_POST['field-name-4'] as $elem)
        $stmt -> execute([$uid,$elem]);

        // TODO: перезаписать данные в БД новыми данными,
    // кроме логина и пароля.
  }
  else {
    // Генерируем уникальный логин и пароль.
    // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
    $login = substr(uniqid(),2,12);
    $pass = substr(md5($_POST['field-email'],2,12));
    // Сохраняем в Cookies.
    setcookie('login', $login);
    setcookie('pass2', $pass);
    $stmt = $db->prepare("INSERT INTO application_v2 SET name = ?, email = ?, DateOfBirth = ?, SPower = ?, After10years = ?, OcenkaPitsi = ?, Gender = ?");
    $stmt -> execute([$_POST['field-name-1'],$_POST['field-email'],$_POST['field-date'],$_POST['field-name-3'],$_POST['field-name-2'],$_POST['radio-group-2'],$_POST['radio-group-1']]);
    $id=$db->lastInsertId();
    $stmt = $db->prepare("INSERT INTO Pizza SET user_id = ?, nomer_pitsi = ?");
    foreach($_POST['field-name-4'] as $elem)
      {
        $stmt -> execute([$id,$elem]);
      }
      $stmt = $db->prepare("INSERT INTO loginpass SET id=?, login = ?, password = ?");
      $stmt -> execute([$id,$login,$pass]);
    // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
    // ...
  }
  // Сохранение в XML-документ.
  // ...

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}
